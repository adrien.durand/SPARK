package consumer

import database.database
import com.datastax.driver.core.utils.UUIDs
import scala.collection.mutable.ArrayBuffer


class Processor {
  val postList = List("userId", "message", "longitude", "latitude", "time")
  val likeList = List("userId", "postId")
  val sendList = List("userId", "receiverId", "message", "time")
  val userList = List("mail", "name", "age")

  val userBuf = new ArrayBuffer[(String, String, String, String)]
  val likeBuf = new ArrayBuffer[(String, String, String)]
  val sendBuf = new ArrayBuffer[(String, String, String, String, String)]
  val postBuf = new ArrayBuffer[(String, String, String, String, String, String)]

  case class IndCol(ind : Int, col : String)

  def getIndFromString(names : List[String], row: Array[String]) : List[IndCol] = {
    names.foldLeft(List[IndCol]())((acc, colName) => IndCol(row.indexOf(colName), colName)::acc)
  }

  def mySplit(str: String, cur: StringBuilder = new StringBuilder(), ind: Int = 0, quoting: Boolean = false,
              res: Array[String] = Array()) : Array[String] = {
    if (ind == str.size)
      res :+ cur.toString()
    else {
      (str(ind), quoting) match {
        case ('"', _) if ind == 0 => mySplit(str, cur, ind + 1, !quoting, res)
        case ('"', _) if str(ind - 1) != '\\' => mySplit(str, cur, ind + 1, !quoting, res)
        case ('"', _) => mySplit(str, cur, ind + 1, quoting, res)
        case (',', true) => mySplit(str, cur.append(','), ind + 1, quoting, res)
        case (',', false) => mySplit(str, new StringBuilder(), ind + 1, quoting, res :+ cur.toString())
        case (c, _) => mySplit(str, cur.append(c), ind + 1, quoting, res)
      }
    }
  }

  def process(data: String): Unit = {
    val lines = data.split(System.lineSeparator())
    val elm = lines(0).split(",")(0)
    if (elm == "post")
      post(lines)
    else if (elm == "like")
      like(lines)
    else if (elm == "send")
      send(lines)
    else if (elm == "user")
      user(lines)
    else
      println("Error: data could not be parsed")
  }

  def getColFromName(list: List[IndCol], name: String) : IndCol = {
    list match {
      case head::Nil => head
      case head::_ if head.col.equals(name) => head
      case _::tail => getColFromName(tail, name)
    }
  }

  def post(data: Array[String]) : Unit = {
    val ind = getIndFromString(postList, mySplit(data(0)))
    val value = mySplit(data(1))
    postBuf.append((UUIDs.timeBased().toString,
      value(getColFromName(ind, "userId").ind),
      value(getColFromName(ind, "message").ind),
      value(getColFromName(ind, "longitude").ind),
      value(getColFromName(ind, "latitude").ind),
      value(getColFromName(ind, "time").ind)))
  }

  def user(data: Array[String]) : Unit = {
    val ind = getIndFromString(userList, mySplit(data(0)))
    val value = mySplit(data(1))
    userBuf.append((UUIDs.timeBased().toString,
      value(getColFromName(ind, "mail").ind),
      value(getColFromName(ind, "name").ind),
      value(getColFromName(ind, "age").ind)))
  }

  def like(data: Array[String]) : Unit = {
    val ind = getIndFromString(likeList, mySplit(data(0)))
    val value = mySplit(data(1))
    likeBuf.append((UUIDs.timeBased().toString,
      value(getColFromName(ind, "userId").ind),
      value(getColFromName(ind, "postId").ind)))
  }

  def send(data: Array[String]) : Unit = {
    val ind = getIndFromString(sendList, mySplit(data(0)))
    val value = mySplit(data(1))
    sendBuf.append((UUIDs.timeBased().toString,
      value(getColFromName(ind, "userId").ind),
      value(getColFromName(ind, "receiverId").ind),
      value(getColFromName(ind, "message").ind),
      value(getColFromName(ind, "time").ind)))
  }

  def execute() : Unit = {
    if (likeBuf.size > 0)
      database.insertLike(likeBuf.toSeq)
    if (sendBuf.size > 0)
      database.insertMessage(sendBuf.toSeq)
    if (postBuf.size > 0)
      database.insertPost(postBuf.toSeq)
    if (userBuf.size > 0)
      database.insertUser(userBuf.toSeq)
  }
}
