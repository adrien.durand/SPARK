package consumer


import java.util.{Properties,Collections}
import scala.collection.JavaConverters._

import org.apache.kafka.clients.consumer.KafkaConsumer

class Consumer {

  val TOPIC="network"

  val  props = new Properties()
  props.put("bootstrap.servers", "localhost:9092")

  props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
  props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
  props.put("group.id", "something")

  val consumer = new KafkaConsumer[Nothing, String](props)

  consumer.subscribe(Collections.singletonList(TOPIC))

  def start() = {
    while(true){
      val processor = new Processor()
      consumer.poll(100).asScala.foreach(x => processor.process(x.value))
      processor.execute()
    }
  }
}
