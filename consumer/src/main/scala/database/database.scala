package database

import com.datastax.spark.connector._
import org.apache.spark.sql.cassandra._

import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.rdd.RDD
import com.datastax.driver.core.utils.UUIDs

object database{

  val conf = new SparkConf(true)
    .setMaster("local[*]")
    .setAppName("SparkSocialNetwork")
    .set("spark.cassandra.connection.host", "localhost")

  val sc = new SparkContext(conf)

  def insertUser(myData: Seq[(String, String, String, String)]) = {
    val collection = sc.parallelize(myData)
    collection.saveToCassandra("sparksocialnetwork", "user", SomeColumns("id", "mail", "name", "age"))
  }

  def insertPost(myData: Seq[(String, String, String, String, String, String)]) = {
    val collection = sc.parallelize(myData)
    collection.saveToCassandra("sparksocialnetwork", "post", SomeColumns("id", "userid", "post", "longitude", "latitude", "time"))
  }

  def insertMessage(myData:Seq[(String, String, String, String, String)]) = {
    val collection = sc.parallelize(myData)
    collection.saveToCassandra("sparksocialnetwork", "message", SomeColumns("id", "userid", "receiverid", "message", "time"))
  }

  def insertLike(myData:Seq[(String, String, String)]) = {
    val collection = sc.parallelize(myData)
    collection.saveToCassandra("sparksocialnetwork", "like", SomeColumns("id", "userid", "postid"))
  }
}
