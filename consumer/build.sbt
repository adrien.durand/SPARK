name := "SparkSocialNetwork"

mainClass in (Compile, run) := Some("main.Main")

version := "0.1"

scalaVersion := "2.11.8"

resolvers += "Spark Packages Repo" at "https://dl.bintray.com/spark-packages/maven"

libraryDependencies ++= Seq(
  "datastax" % "spark-cassandra-connector" % "2.3.0-s_2.11",
  "org.apache.spark" %% "spark-core" % "2.0.0",
  "org.apache.spark" %% "spark-sql" % "2.0.0",
  "org.apache.kafka" % "kafka-clients" % "1.1.0"
)
