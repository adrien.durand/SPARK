package SparkSocialNetwork

import com.datastax.spark.connector.CassandraRow

object term {
  val uri_hdfs = "hdfs://localhost:9000"

  def start() {
    println("What do you want to do ?\n" +
      "\t1- search for companies in messages\n" +
      "\t2- search for companies in posts\n" +
      "\t3- search for companies in both\n" +
      "\t4- search for companies in both from a date\n" +
      "\t5- to dump the database in a hdfs\n" +
      "\t6- Quit")
    val input = scala.io.StdIn.readInt();
    if (input == 1)
      searchMessage()
    else if (input == 2)
      searchPost()
    else if (input == 3)
      searchBoth()
    else if (input == 4)
      searchBothWithDate()
    else if (input == 5)
      CassandraDB.persistsAllToHDFS("sparksocialnetwork")
    else if (input == 6)
      return
    start()
  }

  def searchMessage() {
    println("What company are you searching ?\n")
    val input = scala.io.StdIn.readLine()
    val rddMessages = CassandraDB.searchForInMessages(input)
    println("======== OCCURRENCES OF '"+input+"' IN MESSAGES ========\n")
      rddMessages.foreach(x => {

        val rddUsers = CassandraDB.readFromHDFS(uri_hdfs+"/data/users")

        val userFrom : CassandraRow = rddUsers.filter(user => user.columnValues(user.metaData
                                                .namesToIndex
                                                .getOrElse("id", 0)).toString
                                                .equals(x.columnValues(x.metaData.namesToIndex
                                                .getOrElse("userid", 1)).toString))
                                                .first

        val userTo : CassandraRow = rddUsers.filter(user => user.columnValues(user.metaData
                                                .namesToIndex
                                                .getOrElse("id", 0)).toString
                                                .equals(x.columnValues(x.metaData.namesToIndex
                                                .getOrElse("receiverid", 2)).toString))
                                                .first

      println("\nFrom: " + userFrom.columnValues(userFrom.metaData.namesToIndex
                                         .getOrElse("mail", 2)).toString)
      println("To: " + userTo.columnValues(userTo.metaData.namesToIndex
                                         .getOrElse("mail", 2)))

      println("Date: " + x.columnValues(x.metaData.namesToIndex
                                         .getOrElse("time", 4)))

      println("Content: " + x.columnValues(x.metaData.namesToIndex
                                         .getOrElse("message", 3)))

    }) 
    println()
  }

  def searchPost() {
    println("What company are you searching ?\n")
    val input = scala.io.StdIn.readLine()
    val rddPosts = CassandraDB.searchForInMessages(input)
    println("======== OCCURRENCES OF '"+input+"' IN POSTS ========\n")
    rddPosts.foreach(x => {
      val rddUsers = CassandraDB.readFromHDFS(uri_hdfs+"/data/users")
      val user : CassandraRow = rddUsers.filter(user => user.columnValues(user.metaData
                                                    .namesToIndex
                                                    .getOrElse("id", 0)).toString
                                                    .equals(x.columnValues(x.metaData.namesToIndex
                                                    .getOrElse("userid", 1)).toString))
                                                    .first

      println("\nFrom: " + user.columnValues(user.metaData.namesToIndex
                                          .getOrElse("mail", 2)).toString)
      println("Age: " + user.columnValues(user.metaData.namesToIndex
                                          .getOrElse("age", 3)))
      println("Longitude: " + x.columnValues(x.metaData.namesToIndex
                                          .getOrElse("longitude", 3)))
      println("Latitude: " + x.columnValues(x.metaData.namesToIndex
                                          .getOrElse("latitude", 4)))
      println("Date: " + x.columnValues(x.metaData.namesToIndex
                                          .getOrElse("time", 5)))

      println("Content: " + x.columnValues(x.metaData.namesToIndex
                                          .getOrElse("post", 2)))
      })
    println()
  }

  def searchBoth() {
    println("What company are you searching ?\n")
    CassandraDB.searchFor(scala.io.StdIn.readLine())
  }

  def searchBothWithDate() {
    println("What company are you searching ?\n")
    val comp = scala.io.StdIn.readLine()
    println("Enter a date (dd/MM/yyyy)")
    val date = scala.io.StdIn.readLine()
    CassandraDB.searchForSince(scala.io.StdIn.readLine(), date)
  }
}
