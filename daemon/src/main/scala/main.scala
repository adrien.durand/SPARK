package SparkSocialNetwork

import com.datastax.spark.connector._
import org.apache.spark.sql.cassandra._

import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.rdd.RDD
import com.datastax.driver.core.utils.UUIDs
import java.util.Date
import java.time.Instant

object Main extends App {

    val sc = CassandraDB.sc
    CassandraDB.persistsAllToHDFS("sparksocialnetwork")
    sc.cassandraTable("sparksocialnetwork", "message").deleteFromCassandra("sparksocialnetwork", "message")
    sc.cassandraTable("sparksocialnetwork", "post").deleteFromCassandra("sparksocialnetwork", "post")
    sc.cassandraTable("sparksocialnetwork", "like").deleteFromCassandra("sparksocialnetwork", "like")
    sc.stop()

}
