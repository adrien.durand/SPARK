package SparkSocialNetwork

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd._
import com.datastax.spark.connector.cql.CassandraConnector
import com.datastax.spark.connector.CassandraRow
import com.datastax.spark.connector._
import java.text.SimpleDateFormat
import java.util.Date
/*case class Message(id: Int, from: String, to: String, content: String)
case class Post(id: Int, from: String, content: String,
longitude: Int, latitude: Int, likes: Int)
case class User(id: Int, username: String, email: String, firstname: String,
lastname: String)*/

object CassandraDB {

    val conf = new SparkConf(true)
    .setMaster("local[*]")
    .setAppName("SparkSocialNetwork")
    .set("spark.cassandra.connection.host", "localhost")
    .set("spark.sql.broadcastTimeout", "1200")

    val sc = new SparkContext(conf)
    sc.setLogLevel("ERROR")

    val uri_hdfs = "hdfs://localhost:9000"

    /**     Persitance with HDFS     **/

    /* Persists table in HDFS */
    def persistsTableToHDFS(keyspace: String, table: String, path: String, drop: Boolean) = {
        val fs = org.apache.hadoop.fs.FileSystem.get(new java.net.URI(uri_hdfs), sc.hadoopConfiguration)
        if(fs.exists(new org.apache.hadoop.fs.Path(path)) && drop) {
            val rddOld = readFromHDFS(uri_hdfs+path)
            rddOld.saveToCassandra(keyspace, table)

            fs.delete(new org.apache.hadoop.fs.Path(path),true)
            val rddCass = sc.cassandraTable(keyspace, table)
            rddCass.saveAsObjectFile(uri_hdfs+path)
        } else if (fs.exists(new org.apache.hadoop.fs.Path(path)) && !drop) {
            fs.delete(new org.apache.hadoop.fs.Path(path),true)
            val rddCass = sc.cassandraTable(keyspace, table)
            rddCass.saveAsObjectFile(uri_hdfs+path)
        } else {
            val rddCass = sc.cassandraTable(keyspace, table)
            rddCass.saveAsObjectFile(uri_hdfs+path)
        }
    }

    /* Persits Cassandra Rows of each tables in HDFS */
    def persistsAllToHDFS(keyspace: String) = {
        persistsTableToHDFS(keyspace, "user", "/data/users", false)
        persistsTableToHDFS(keyspace, "message", "/data/messages", true)
        persistsTableToHDFS(keyspace, "post", "/data/posts", true)
        persistsTableToHDFS(keyspace, "like", "/data/likes", true)
    }

    /* Read from HDFS tables */
    def readFromHDFS(path: String) : RDD[CassandraRow]= {
        sc.objectFile(path)
    }


    /**     SEARCH ENGINE   **/

    def searchForInPosts(input: String) : RDD[CassandraRow] = {
        val rdd = readFromHDFS(uri_hdfs+"/data/posts")
        rdd.filter(x => x.columnValues(x.metaData.namesToIndex
                                        .getOrElse("post", 0)).toString
                                        .toLowerCase()
                                        .contains(input.toLowerCase()))
    }

    def searchForInMessages(input: String) : RDD[CassandraRow] = {
        val rdd = readFromHDFS(uri_hdfs+"/data/messages")
        rdd.filter(x => x.columnValues(x.metaData.namesToIndex
                                        .getOrElse("message", 0)).toString
                                        .toLowerCase()
                                        .contains(input.toLowerCase()))
    }

    def searchForSince(input: String, strDate: String) = {

        val date: Date = strToDate(strDate)

        val rddPosts = searchForInPosts(input).filter(x => {
            x.columnValues(x.metaData
             .namesToIndex
             .getOrElse("time", 5)).asInstanceOf[Date].after(date)
        })

        val rddMessages = searchForInMessages(input).filter(x => {
            x.columnValues(x.metaData
             .namesToIndex
             .getOrElse("time", 4)).asInstanceOf[Date].after(date)
        })

        println("======== OCCURRENCES OF '"+input+"' IN POSTS ========\n")
        rddPosts.foreach(x => {
            val rddUsers = readFromHDFS(uri_hdfs+"/data/users")
            //val rddLikes = readFromHDFS(uri_hdfs+"/data/likes")
            val user : CassandraRow = rddUsers.filter(user => user.columnValues(user.metaData
                                                    .namesToIndex
                                                    .getOrElse("id", 0)).toString
                                                    .equals(x.columnValues(x.metaData.namesToIndex
                                                    .getOrElse("userid", 1)).toString))
                                                    .first
            //val likes : RDD[CassandraRow] = rddLikes.
            println("\nFrom: " + user.columnValues(user.metaData.namesToIndex
                                             .getOrElse("mail", 2)).toString)
            println("Age: " + user.columnValues(user.metaData.namesToIndex
                                             .getOrElse("age", 3)))
            println("Longitude: " + x.columnValues(x.metaData.namesToIndex
                                             .getOrElse("longitude", 3)))
            println("Latitude: " + x.columnValues(x.metaData.namesToIndex
                                             .getOrElse("latitude", 4)))
            println("Date: " + x.columnValues(x.metaData.namesToIndex
                                             .getOrElse("time", 5)))

            println("Content: " + x.columnValues(x.metaData.namesToIndex
                                                .getOrElse("post", 2)))

            /*println("Likes: " + x.columnValues(x.metaData.namesToIndex
                                                  .getOrElse("post", 2)))*/

        })
        println("")
        println("======== OCCURRENCES OF '"+input+"' IN MESSAGES ========\n")
        rddMessages.foreach(x => {

            val rddUsers = readFromHDFS(uri_hdfs+"/data/users")

            val userFrom : CassandraRow = rddUsers.filter(user => user.columnValues(user.metaData
                                                    .namesToIndex
                                                    .getOrElse("id", 0)).toString
                                                    .equals(x.columnValues(x.metaData.namesToIndex
                                                    .getOrElse("userid", 1)).toString))
                                                    .first

            val userTo : CassandraRow = rddUsers.filter(user => user.columnValues(user.metaData
                                                    .namesToIndex
                                                    .getOrElse("id", 0)).toString
                                                    .equals(x.columnValues(x.metaData.namesToIndex
                                                    .getOrElse("receiverid", 2)).toString))
                                                    .first

            println("\nFrom: " + userFrom.columnValues(userFrom.metaData.namesToIndex
                                             .getOrElse("mail", 2)).toString)

            println("To: " + userTo.columnValues(userTo.metaData.namesToIndex
                                             .getOrElse("mail", 2)))

            println("Date: " + x.columnValues(x.metaData.namesToIndex
                                             .getOrElse("time", 4)))

            println("Content: " + x.columnValues(x.metaData.namesToIndex
                                                .getOrElse("message", 3)))

        })
        println("")
        println("======================  FINAL COUNT FOR '"+input+"' SINCE "+ strDate +"  ======================\n")
        val postCnt = rddPosts.count
        val msgCnt = rddMessages.count
        val total = msgCnt + postCnt
        println("Appeared in "+postCnt+" posts")
        println("Appeared in "+msgCnt+" messages\n")
        println("Total: "+total+" times\n")

    }


    def searchFor(input: String) = {
        val rddPosts = searchForInPosts(input)
        val rddMessages = searchForInMessages(input)
        println("======== OCCURRENCES OF '"+input+"' IN POSTS ========\n")
        rddPosts.foreach(x => {
            val rddUsers = readFromHDFS(uri_hdfs+"/data/users")
            //val rddLikes = readFromHDFS(uri_hdfs+"/data/likes")
            val user : CassandraRow = rddUsers.filter(user => user.columnValues(user.metaData
                                                    .namesToIndex
                                                    .getOrElse("id", 0)).toString
                                                    .equals(x.columnValues(x.metaData.namesToIndex
                                                    .getOrElse("userid", 1)).toString))
                                                    .first
            //val likes : RDD[CassandraRow] = rddLikes.
            println("\nFrom: " + user.columnValues(user.metaData.namesToIndex
                                             .getOrElse("mail", 2)).toString)
            println("Age: " + user.columnValues(user.metaData.namesToIndex
                                             .getOrElse("age", 3)))
            println("Longitude: " + x.columnValues(x.metaData.namesToIndex
                                             .getOrElse("longitude", 3)))
            println("Latitude: " + x.columnValues(x.metaData.namesToIndex
                                             .getOrElse("latitude", 4)))
            println("Date: " + x.columnValues(x.metaData.namesToIndex
                                             .getOrElse("time", 5)))

            println("Content: " + x.columnValues(x.metaData.namesToIndex
                                                .getOrElse("post", 2)))

            println("Likes: " + x.columnValues(x.metaData.namesToIndex
                                                  .getOrElse("post", 2)))

        })
        println("")
        println("======== OCCURRENCES OF '"+input+"' IN MESSAGES ========\n")
        rddMessages.foreach(x => {

            val rddUsers = readFromHDFS(uri_hdfs+"/data/users")

            val userFrom : CassandraRow = rddUsers.filter(user => user.columnValues(user.metaData
                                                    .namesToIndex
                                                    .getOrElse("id", 0)).toString
                                                    .equals(x.columnValues(x.metaData.namesToIndex
                                                    .getOrElse("userid", 1)).toString))
                                                    .first

            val userTo : CassandraRow = rddUsers.filter(user => user.columnValues(user.metaData
                                                    .namesToIndex
                                                    .getOrElse("id", 0)).toString
                                                    .equals(x.columnValues(x.metaData.namesToIndex
                                                    .getOrElse("receiverid", 2)).toString))
                                                    .first

            println("\nFrom: " + userFrom.columnValues(userFrom.metaData.namesToIndex
                                             .getOrElse("mail", 2)).toString)

            println("To: " + userTo.columnValues(userTo.metaData.namesToIndex
                                             .getOrElse("mail", 2)))

            println("Date: " + x.columnValues(x.metaData.namesToIndex
                                             .getOrElse("time", 4)))

            println("Content: " + x.columnValues(x.metaData.namesToIndex
                                                .getOrElse("message", 3)))

        })
        println("")
        println("======================  FINAL COUNT FOR '"+input+"'  ======================\n")
        val postCnt = rddPosts.count
        val msgCnt = rddMessages.count
        val total = msgCnt + postCnt
        println("Appeared in "+postCnt+" posts")
        println("Appeared in "+msgCnt+" messages\n")
        println("Total: "+total+" times\n")
    }

    def strToDate(str: String): Date = {
        val format = new java.text.SimpleDateFormat("dd/MM/yyyy");
        format.parse(str);
    }

}
