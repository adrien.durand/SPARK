package SparkSocialNetwork

import com.datastax.spark.connector._
import org.apache.spark.sql.cassandra._
import com.datastax.driver.core._
import com.datastax.spark.connector.cql.CassandraConnector
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import com.datastax.driver.core.utils.UUIDs

object Main extends App {
  val conf = new SparkConf(true)
    .setMaster("local[*]")
    .setAppName("SparkSocialNetwork")
    .set("spark.cassandra.connection.host", "localhost")

  val sc = new SparkContext(conf)
  val sebid = UUIDs.timeBased().toString
  val maxid = UUIDs.timeBased().toString
  val postid = UUIDs.timeBased().toString
  CassandraConnector(sc.getConf).withSessionDo{ session =>
    session.execute("DROP KEYSPACE IF EXISTS sparksocialnetwork;")
    session.execute("CREATE KEYSPACE sparksocialnetwork WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1 };")
    session.execute("DROP TABLE IF EXISTS sparksocialnetwork.post;")
    session.execute("DROP TABLE IF EXISTS sparksocialnetwork.like;")
    session.execute("DROP TABLE IF EXISTS sparksocialnetwork.user;")
    session.execute("DROP TABLE IF EXISTS sparksocialnetwork.message;")
    session.execute("CREATE TABLE sparksocialnetwork.user(id uuid PRIMARY KEY, name text, mail text, age int);")
    session.execute("CREATE TABLE sparksocialnetwork.message(id uuid PRIMARY KEY, userId uuid, receiverId uuid, message text, time timestamp);")
    session.execute("CREATE TABLE sparksocialnetwork.like(id uuid PRIMARY KEY, userId uuid, postId uuid);")
    session.execute("CREATE TABLE sparksocialnetwork.post(id uuid PRIMARY KEY, userId uuid, post text, longitude float, latitude float, time timestamp);")
    session.execute("INSERT INTO sparksocialnetwork.user(id, name, mail, age) VALUES (" +  sebid + ", 'Sebastien','fauresebast@gmail.com', 21);")
    session.execute("INSERT INTO sparksocialnetwork.user(id, name, mail, age) VALUES (" + maxid + ", 'Maxime','maxime.bertrait@gmail.com', 22);")
    session.execute("INSERT INTO sparksocialnetwork.user(id, name, mail, age) VALUES (" + UUIDs.timeBased().toString + ", 'Sebastien','adrien.943@gmail.com', 22);")
    session.execute("INSERT INTO sparksocialnetwork.post(id, userId, post, longitude, latitude, time) VALUES ("
      + UUIDs.timeBased().toString + ", " + sebid + ", 'France championne du monde !', 22., 23., '1998-07-12');")
    session.execute("INSERT INTO sparksocialnetwork.post(id, userId, post, longitude, latitude, time) VALUES ("
      + postid + ", " + maxid + ", 'Mon premier iPhone ! Merci Apple', 42., 42., '2010-12-25');")
    session.execute("INSERT INTO sparksocialnetwork.post(id, userId, post, longitude, latitude, time) VALUES ("
      + UUIDs.timeBased().toString + ", " + maxid + ", 'Le feu le bucket de KFC ! Mardi = best day of the week', 26., 32., '2015-10-10');")
    session.execute("INSERT INTO sparksocialnetwork.like(id, userid, postId) VALUES (" + UUIDs.timeBased().toString + ", " + sebid + ", " + postid + ");")
    session.execute("INSERT INTO sparksocialnetwork.message(id, userid, receiverId, message, time) VALUES (" + UUIDs.timeBased().toString + ", " + sebid + ", " + maxid + ", 'Je peux tester ? Go manger KFC que je vois ça!', '2010-12-25');")
    session.execute("INSERT INTO sparksocialnetwork.message(id, userid, receiverId, message, time) VALUES (" + UUIDs.timeBased().toString + ", " + maxid + ", " + sebid + ", 'Je préfère faire un Burger King cette fois', '2010-12-25');")
  }

}
