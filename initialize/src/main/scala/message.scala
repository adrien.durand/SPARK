package SparkSocialNetwork

case class Message(id: Int, author: String, text: String)
