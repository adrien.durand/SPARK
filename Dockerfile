FROM ubuntu 
MAINTAINER adrien.durand@epita.fr

RUN apt-get update && apt-get install -y openjdk-8-jre-headless wget ssh rsync
RUN wget http://apache.mediamirrors.org/zookeeper/zookeeper-3.4.12/zookeeper-3.4.12.tar.gz
RUN tar -xzf zookeeper-3.4.12.tar.gz -C /opt
RUN mv /opt/zookeeper-3.4.12 /opt/zookeeper && cp /opt/zookeeper/conf/zoo_sample.cfg /opt/zookeeper/conf/zoo.cfg
RUN wget http://apache.crihan.fr/dist/kafka/1.1.0/kafka_2.11-1.1.0.tgz
RUN tar -xzf kafka_2.11-1.1.0.tgz -C /opt
RUN wget http://apache.mediamirrors.org/cassandra/3.11.2/apache-cassandra-3.11.2-bin.tar.gz
RUN tar -xzf apache-cassandra-3.11.2-bin.tar.gz -C /opt
RUN wget http://apache.crihan.fr/dist/hadoop/common/hadoop-3.0.3/hadoop-3.0.3.tar.gz
RUN tar -xzf hadoop-3.0.3.tar.gz -C /opt
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
EXPOSE 2181 2888 3888 9092
CMD echo "<configuration> <property> <name>fs.defaultFS</name> <value>hdfs://localhost:9000</value> </property> </configuration>" > /etc/hadoop/core-site.xml
CMD echo "<configuration> <property> <name>dfs.replication</name> <value>1</value> </property> </configuration>" > /etc/hadoop/hdfs-site.xml
CMD /opt/zookeeper/bin/zkServer.sh start && opt/kafka_2.11-1.1.0/bin/kafka-server-start.sh /opt/kafka_2.11-1.1.0/config/server.properties
