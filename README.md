=== AUTHORS ===
BERTRAIT Maxime - maxime.bertrait@epita.fr
DURAND Adrien - adrien.durand@epita.fr
FAURE Sébastien - sebastien.faure@epita.fr

=== THE PROJECT ===
Manage the backend part of a social network with Spark, Cassandra and Kafka

=== INSTALLATION ===
To run this project, you need:
Cassandra, Docker, an HDFS configuration and Kafka

We are giving you al the documentation we followed to have the right configuration for our project.

=== DOCUMENTATION ===
Cassandra: http://cassandra.apache.org/
Cassandra Connector: https://github.com/datastax/spark-cassandra-connector
Docker: https://www.docker.com/
Hadoop (for HDFS): https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-common/SingleCluster.html#Pseudo-Distributed_Operation 
Kafka: http://kafka.apache.org/


=== HOW TO INITIALIZE THE PROJECT === 
1. start your cassandra database on localhost
  sh$ $CASSANDRA_HOME/bin/cassandra -f

2. Initialize the database with our solution
  sh$ cd initialize
  sh$ sbt run

3. daemon to make cassandra database saved on HDFS database

4. Start kafka

5. Run the producer in the "producer" folder
  sh$ cd producer
  sh$ sbt run

6. Run the consumer in the "consumer" folder
  sh$ cd consumer
  sh$ sbt run

7. Start the solution in the "search" folder
  sh$ cd search
  sh$ sbt run

  You can now do research from our database

  You also need to run: crontab -e And add inside editor */15 * * * * pathToRoot/daemon.sh to schedule
the daemon. Every 15 minutes it will run the specified script which persists the DB in
HDFS and erase it.
