package terminal

import producer.Producer

class MyTerm (producer: Producer) {
  def start() : Unit = {
    println("What do you want to do ?\n" +
      "\t1- create a user\n" +
      "\t2- create a post\n" +
      "\t3- send a message\n" +
      "\t4- like a post\n" +
      "\t5- nothing")
    val input = scala.io.StdIn.readInt();
    if (input == 1)
      create()
    else if (input == 2)
      post()
    else if (input == 3)
      send()
    else if (input == 4)
      like()
    else if (input == 5) {
      producer.close()
      return
    }
    start()
  }

  def create() = {
    println("What is your mail ?")
    val mail = scala.io.StdIn.readLine()
    println("What is your name ?")
    val name = scala.io.StdIn.readLine()
    println("What is your age ?")
    val age = scala.io.StdIn.readInt()
    producer.createUser(mail, name, age)
  }

  def like() = {
    println("What is your id ?")
    val userId = scala.io.StdIn.readLine()
    println("Which post id would you like ?")
    val postId = scala.io.StdIn.readLine()
    producer.sendLike(userId, postId)
  }

  def send() = {
    println("What is your id ?")
    val userId = scala.io.StdIn.readLine()
    println("To whom would you like to write ?")
    val receiverId = scala.io.StdIn.readLine()
    println("Which message would you send ?")
    val message = scala.io.StdIn.readLine()
    producer.sendMessage(userId,receiverId,message)
  }

  def post() = {
    println("What is your id ?")
    val userId = scala.io.StdIn.readLine()
    println("Which message would you post ?")
    val message = scala.io.StdIn.readLine()
    println("What is your longitude ?")
    val longitude = scala.io.StdIn.readInt()
    println("What is your latitude ?")
    val latitude = scala.io.StdIn.readInt()
    producer.sendPost(userId, message, longitude, latitude)
  }
}
