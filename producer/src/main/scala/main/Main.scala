package main

import producer.Producer
import terminal.MyTerm

object Application extends App {
  val producer = new Producer()
  new MyTerm(producer).start()
  producer.close()
}
