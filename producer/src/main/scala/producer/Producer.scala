package producer

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import java.time

class Producer {

  val props = new Properties
  props.put("bootstrap.servers", "localhost:9092")

  props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")

  val producer = new KafkaProducer[Nothing, String](props)

  val topic = "network"

  def sendPost(userId : String, post: String, longitude: Int, latitude: Int) = {
    val time = java.time.LocalDate.now().toString()
    val toSend = "post,userId,message,longitude,latitude,time\n,%s,%s,%d,%d,%s".format(userId,post,longitude,latitude,time)
    val record = new ProducerRecord(topic, toSend)
    producer.send(record)
    println("post : " + toSend + "\n")
  }

  def sendLike(userId : String, postId : String) = {
    val toSend = "like,userId,postId\nlike,%s,%s".format(userId, postId)
    val record = new ProducerRecord(topic, toSend)
    producer.send(record)
    println("like : " + toSend + "\n")
  }

  def createUser(mail: String, name: String, age: Int) = {
    val toSend = "user,mail,name,age\n,%s,%s,%s".format(mail, name, age)
    val record = new ProducerRecord(topic, toSend)
    producer.send(record)
    println("user : " + toSend + "\n")
  }

  def sendMessage(userId : String, receiverId : String, message : String) = {
    val time = java.time.LocalDate.now().toString()
    val toSend = "send,userId,receiverId,message,time\n,%s,%s,%s,%s".format(userId,receiverId,message,time)
    val record = new ProducerRecord(topic, toSend)
    producer.send(record)
    println("send : " + toSend + "\n")
  }

  def close() = {
    producer.close()
  }
}
