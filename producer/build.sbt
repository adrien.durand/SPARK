import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.example",
      scalaVersion := "2.12.6",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "socialnetwork",
    libraryDependencies += scalaTest,
    libraryDependencies += kafka,
    libraryDependencies += "org.slf4j" % "slf4j-log4j12" % "1.7.25"
  )
